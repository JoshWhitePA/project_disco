
public class Color_Values {
	int r,g,b,count;
	int[] colorVals;
	//colorVals = new int[3];

	
	Color_Values() {
		r=0;
		g=0;
		b=0;

		count=0;
		colorVals = new int[3];
		colorVals[0] = 0;
		colorVals[1] = 0;
		colorVals[2] = 0;
	}
	
	int[] calculate(int finalCount){
		//called after all colors have been added
		count = finalCount;
		int temp = 0;
		temp = (r/count);
		//System.out.println(temp);
		setColorVals(0,temp);
		
		temp = 0;
		temp = (g/count);
		setColorVals(1,temp);
		
		temp = 0;
		temp = (b/count);
		setColorVals(2,temp);

		 wordCombination();
		return colorVals;
	}
	
	boolean between(int x, int min, int max) {
		  return x >= min && x <= max;
		}
	
	void addColorsTogether(int red, int green, int blue, int count){
		addR(red);
		addG(green);
		addB(blue);
		addCount(count);
	}
	
	
	void addR(int rVal){
	    r += rVal;
	}

	void addG(int gVal) {
	  g += gVal;
	}

	void addB (int bVal){
	      b +=  bVal;
	}

	
	void setR(int rVal){
	    r = rVal;
	}

	void setG(int gVal) {
	  g = gVal;
	}

	void setB (int bVal){
	   b =  bVal;
	}
	
	void setCount(int theCount){
		count = theCount; //ah ah ah
	}
	void addCount(int theCount){
		count += theCount;
	}
	
	int[] getColorVals(){
		return colorVals;
	}
	
	void setColorVals(int idx, int value){
		colorVals[idx] = value;
	}
	
	int getColorVals(int idx){
		return colorVals[idx];
	}
	
	void fillColorVal (){
	     colorVals[0] = r;
	     colorVals[1] = g;
	     colorVals[2] = b;
	}
	
	void setAllZero(){
		 colorVals[0] = 0;
	     colorVals[1] = 0;
	     colorVals[2] = 0;

	     setCount(0);
	     setR(0);
	     setG(0);
	     setB(0);
	}
	
	int getR(){
		return r;
	}
	int getG(){
		return g;
	}
	int getB(){
		return b;
	}
	
	int getIndexedColorVals(int idx){
		return colorVals[idx];
	}
	
	String wordCombination(){//trigger after final amounts are calculated
		
		String rTemp,gTemp,bTemp;
		if (getIndexedColorVals(0) < 100 && getIndexedColorVals(0) > 9){
			rTemp = 0 + Integer.toString(getIndexedColorVals(0));
		}
		else if (getIndexedColorVals(0) < 10){
			rTemp = 0+(0+Integer.toString(getIndexedColorVals(0)));
		}
		else
		{
			rTemp = Integer.toString(getIndexedColorVals(0));
		}
		
		if (getIndexedColorVals(1) < 100 && getIndexedColorVals(1) > 9){
			gTemp = 0 + Integer.toString(getIndexedColorVals(1));
		}
		else if (getIndexedColorVals(1) < 10){
			gTemp = 0+(0+Integer.toString(getIndexedColorVals(1)));
		}
		else {
			gTemp = Integer.toString(getIndexedColorVals(1));
		}
		
		if (getIndexedColorVals(2) < 100 && getIndexedColorVals(2) > 9){
			bTemp = 0 + Integer.toString(getIndexedColorVals(2));
		}
		else if (getIndexedColorVals(2) < 10){
			bTemp = 0+(0+Integer.toString(getIndexedColorVals(2)));
		}
		else {
			bTemp = Integer.toString(getIndexedColorVals(2));
		}
		return rTemp+gTemp+bTemp;
	}
	
}
